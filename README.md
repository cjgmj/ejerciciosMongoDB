# utilidadesMongoDB

## Índice

1. [Comandos básicos](./contenido/comandos-basicos.md)
2. [Conceptos básicos](./contenido/conceptos-basicos.md)
3. [Esquemas y relaciones](./contenido/esquemas-relaciones.md)
4. [Configuración mongod](./contenido/configuracion-mongod.md)
5. [Configuración mongo](./contenido/configuracion-mongo.md)
6. [Índices](./contenido/indices.md)
7. [Datos geoespaciales](./contenido/datos-geoespaciales.md)
8. [Agregaciones](./contenido/agregaciones.md)
9. [Valores numéricos](./contenido/valores-numericos.md)
10. [Seguridad](./contenido/seguridad.md)
11. [Rendimiento y tolerancia a fallos](./contenido/rendimiento-tolerancia-fallos.md)
12. [Transacciones](./contenido/transacciones.md)
13. [MongoDB Realm](./contenido/mongodb-realm.md)

---

[Documentación MongoDB](https://docs.mongodb.com/)

[Guardar configuración en fichero](https://docs.mongodb.com/manual/reference/configuration-options/)

[Configuración mongod](https://docs.mongodb.com/manual/reference/program/mongod/)

[Configuración mongo](https://docs.mongodb.com/manual/reference/program/mongo/)

[Operadores mongo](https://docs.mongodb.com/manual/reference/operator/)

[Búsqueda con find](https://docs.mongodb.com/manual/reference/method/db.collection.find/)

[Iterar un cursor](https://docs.mongodb.com/manual/tutorial/iterate-a-cursor/)

[Índices](https://docs.mongodb.com/manual/indexes/)

[Índices parciales](https://docs.mongodb.com/manual/core/index-partial/)

[Diferentes lenguajes en el mismo índice](https://docs.mongodb.com/manual/tutorial/specify-language-for-text-index/#create-a-text-index-for-a-collection-in-multiple-languages)

[Consultas geoespaciales](https://docs.mongodb.com/manual/geospatial-queries/)

[Operadores geoespaciales](https://docs.mongodb.com/manual/reference/operator/query-geospatial/)

[Agregación](https://docs.mongodb.com/manual/core/aggregation-pipeline/)

[Funciones de agregación](https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline/)

[Operadores de agregación](https://docs.mongodb.com/manual/reference/operator/aggregation/)

[Optimizar agregaciones](https://docs.mongodb.com/manual/core/aggregation-pipeline-optimization/)

[Datos monetarios](https://docs.mongodb.com/manual/tutorial/model-monetary-data/)

[Colecciones limitadas](https://docs.mongodb.com/manual/core/capped-collections/)

[Conjuntos de réplicas](https://docs.mongodb.com/manual/replication/)

[Configurar conjuntos de réplicas](https://docs.mongodb.com/manual/reference/replica-configuration/)

[Fragmentación](https://docs.mongodb.com/manual/sharding/)

[Transacciones](https://docs.mongodb.com/manual/core/transactions/)
