# Índices

Los índices se utilizan para hacer consultas, actualizacion o eliminaciones, es decir, todas las acciones que puedan contener filtros, de forma más rápida.

En consultas donde no existe índice, se recorrerá toda la colección comproban en cada documento si el parámetro del filtro coincide. Al crear un índice se guardará en una lista ordenada todos los valores posibles en la colección. Cada valor de la lista tiene un puntero al índice del documento al que pertenece. Al tener una lista ya ordenada como respuesta el método `sort` también se ejecutará más rápido, siempre que se ordene por el mismo campo que el filtro.

Si una colección tiene demasiados índices se verá afectada la velocidad con la que se insertan y actualizan documentos en esa colección, ya que al insertar o modificar el documento se actualizaría cada uno de los índices.

Si se realiza una consulta por un campo que está indexado pero ningún valor coincide con el filtro la consulta irá más lenta, ya que tiene que consultar primero por el índice y después consultará la colección completa.

Los índices, mayormente, son útiles cuando la consulta devuelve pocos documentos de la colección. Si la consulta devuelve todos los documentos o casi todos los documentos, los índices no serán útil. No tiene mucho sentido crear índices para booleanos porque al tener solo dos valores posibles no acelera mucho la consulta.

## Examinar consulta

Se puede comprobar si un índice es útil o no examinando la consulta que se realiza. Esto se realiza mediante el método `explain`, este se introduce entre la colección y la búsqueda `db.collection.explain().find(filter)`. El método `explain` puede ser usado para consultar, modificar o eliminar, no se puede usar para insertar. Al ejecutar esta sentencia, devolverá una descripción detallada de como obtulo los resultados. Se puede obtener una descripción aún más detallada pasando por parámetros al método `explain` `"executionStats"`, `db.collection.explain("executionStats").find(filter)`. Se pueden pasar tres argumentos diferentes:

- "queryPlanner": muestra un resumen de la consulta ejecutada y el plan ganador.
- "executionStats": muestra un resumen de la consulta ejecutada, el plan ganador y los posibles planes rechazados.
- "allPlansExecution": muestra un resumen de la consulta ejecutada, el plan ganador y las decisiones del proceso del plan ganador.

Para saber si un índice es eficiente hay que ver el tiempo en milisegundos del proceso, el tipo de consulta que realizó, por índice (`IXSCAN`) o por columna (`COLLSCAN`), y comparar el número de claves examinados en el índice, el número de documentos examinados y los documentos que se devuelven.

Puede darse el caso que no se examine ningún documento cuando el documento solo tiene un campo y el `_id` y mediante proyección se indique que solo se devuelva el campo. Esto pasa porque ese campo ya se encuentra en el índice por lo que no tiene que buscar en la colección para devolver el valor.

MongoDB puede rechazar algún plan, por ejemplo, si se consulta por dos campos y hay creados dos índices, uno con uno de los campos de la consulta y otro con los dos campos. En este caso, MongoDB rechazará el índice que solo afecta a uno de los dos campos de la consulta y el plan ganador será el índice que afecta a los dos campos.

Para decidir cual es el plan ganador para una consulta, MongoDB hace una pequeña prueba de todos los planes disponibles para ver cual es el que devuelve los resultados más rápidos. Este plan ganador se almacena en caché para esa consulta específica. Esta caché se refresca cuando se han insertados muchos documentos nuevos en la colección, cuando un índice es reconstruido, cuando se añaden o eliminan otros índices o cuando el servidor de MongoDB se reinicia.

## Ver índices

Se puede ver los índices creados para una colección con el método `getIndexes()`, `db.collection.getIndexes()`. Esta función devolverá la lista de todos los índices de la colección, la cual mínimo contendrá el índice para el campo `_id`, que es creado por defecto por MongoDB.

## Crear índice

Para agregar un índice en una colección se usa el método `createIndex`. El método recibe por parámetros un objeto en el que la clave es el campo en el que se quiere crear el índice, se pueden crear índices dentro de documentos embebidos concatenando los nombres con puntos, y el valor es si se deben crear en orden ascendente (`1`) o descendente (`-1`), `db.collection.createIndex({"field": 1})`.

## Eliminar índice

Para eliminar un índice de una colección se usa el método `dropIndex`. El método recibe por parámetros el mismo objeto que se usó para crearlo, `db.collection.dropIndex({"field": 1})`.

## Índices compuestos

Un índice puede afectar solo a una columna o a un grupo de ellas. Los valores compuestos son utilizados de los campos más a la izquierda, por ejemplo si se tiene un índice compuesto y se filtra solo por el primer campo del índice este será utilizado. Sin embargo, si se filtra por el segundo campo del índice sin introducir la primera, no se usará el índice y se filtrará por columnas. Si en la consulta se pasan los dos campos que forman el índice no es necesario que estén en el mismo orden con el que se creó. Un índice puede agrupar hasta 31 columnas, y será utilizado siempre que los filtros añadidos respeten el orden con el que se creó el índice.

## Ordenación con índices

Los índices no solo son útiles para hacer consultas filtradas más rápidas, sino que también son útiles para ordenar la consulta. Es posible que se tenga que realizar una consulta ordenada a una colección, si esta colección es demasiado grande puede agotar el tiempo de espera que tiene MongoDB, ya que para la ordenación tiene un umbral de 32 MB de memoria. Si no tiene índices, MongoBD buscará todos los documentos en la memoria y hará la ordenación en la misma, lo cual para grandes colecciones puede tardar demasiado y de error.

## Configurar índice

A la hora de crear un índice se le puede pasar como segundo argumento una serie de configuraciones. Por ejemplo, se puede indicar uno de los campos como único pasando como segundo argumento `{unique: true}`. Si la colección entra en conflicto con la configuración indicada, dará error y no se creará el índice. Con el índice creado con la configuración de único, si se intenta insertar un nuevo documento que tenga el campo definido como único igual que un documento ya insertado, dará error y no se insertará.

## Filtro parcial

Estos índices solo afectarán a los valores de la columna definidos, de esta manera ahorra espacio ya que es posible que el índice no se use para todos los valores pero sí está ocupando espacio. Para crear estos índices, como segundo argumento se indica una expresión de filtro parcial. Un ejemplo para la creación de un índice con filtro parcial sería `db.collection.createIndex({"field": 1}, {partialFilterExpression: {"field": "filters"}})`. El campo del filtro parcial puede ser diferente del campo del índice. Para aplicar este índice con filtro parcial, la consulta se debe hacer con el mismo filtro. La única diferencia entre estos índices y los índices generales es el espacio ocupado.

## Índice Time-To-Live (TTL)

Este tipo de índices son interesantes para aplicaciones en las que se tienen datos autodestructivos, como por ejemplo sesiones de usuarios las cuales se borrarán después de una duración determinada. Un ejemplo para la creación de este índice sería `db.collection.createIndex({"field": 1}, {expireAfterSeconds: seconds})`. Estos tipos de índices solo funcionan en índices o campos de fechas. Este índice eliminará de la colección los documentos una vez transcurridos los segundos indicados desde el valor del campo del índice.

## Índice Multi-Key

Son creados cuando el índice se crea sobre un campo que contiene una lista. Para crear estos índices, MongoDB extrae todos los valores de la lista y los almacena como elementos separados en un índice. Esto significa que los índices sobre campos que contienen listas son más grandes que los índices sobre campos planos. Estos índices no funcionan con campos que contengan listas de objetos si se filtra por el campo concatenado por el punto, en ese caso consultaría por colección. Para usar el índice, la consulta se debe realizar con la estructura del documento, ya que se crea el índice sobre los documentos. Si se quiere crear el índice sobre un campo concreto del documento, se debe indicar concatenando los campos. Se puede crear un índice con un campo que contenga una lista y otro campo plano pero no apuntando a dos campos que contengan listas.

## Índice de texto

Es un tipo especial de índice Multi-Key, sirve para convertir un campo de texto plano en una lista de palabras, para evitar así hacer búsquedas con expresiones regulares las cuales no tienen un buen rendimiento. Este índice no almacena todas las palabras del campo, solo almacena las palabras claves. Para crear este tiempo de índice se crearía con `db.collection.createIndex({"field": "text"})`. Si se pasan dos palabras separadas buscará los documentos que contenga una de las dos palabras. Para buscar por una frase completa, hay que introducir el texto entre comillas escapando las comillas (`\"phrase\"`). Para hacer uso de este índice la consulta debe ser `db.collection.find({$text: {$search: "wordToFind"}})`. No hace falta pasar sobre que campo se va a filtrar ya que solo es posible tener un índice de texto por colección, aunque si es posible combinar varios campos dentro de un índice de texto.

Se pueden ordernar los resultados pasando como segundo parámetro, para hacer uso de la proyección, `{score: {$meta: "textScore"}}`, de esta forma mostrará primero los resultados que más coincidencia tenga con el texto introducido. Al hacer esto, MongoDB asigna una puntuación a los elementos, la cual es devuelta en la consulta. Con esto se podrá llamar al método `sort` pasando como parámetro `{score: {$meta: "textScore"}}`.

### Índice de texto combinado

Para crear un índice de texto combinado que afecte a varios campos basta con separar los campos por comas teniendo como valor `"text"`.

### Excluir palabras

Para excluir palabras, dentro del texto del filtro hay que añadir `-excludedWord`, `db.collection.find({$text: {$search: "wordToFind -excludedWord"}})`. El signo `-` significa que la palabra debe excluirse.

### Case sensitive

Es posible hacer búsquedas por palabras sin tener en cuenta mayúsculas y minúsculas con `$caseSensitive`, el cual espera un valor booleano, `db.collection.find({$text: {$search: "wordToFind", $caseSensitive: false}})`.

### Configurar idioma

En los índices de texto combinado es posible establecer el idioma (`default_language`), pasándolo como segundo parámetro. El idioma por defecto es inglés, pero es posible cambiarlo por uno de los [idiomas permitidos](https://docs.mongodb.com/manual/reference/text-search-languages/#std-label-text-search-languages). Las palabras que se omiten de una frase a la hora de crear el índice dependen del idioma establecido. También se puede especificar el idioma a la hora de hacer la búsqueda, `db.collection.find({$text: {$search: "wordToFind", $language: "language"}})`.

### Configurar peso de los campos

En los índices de texto combinado es posible determina cual de los campos tiene más peso a la hora de hacer la búsqueda, lo cual afecta a la puntuación generada por MongoDB. Para definir esto hay que pasar como segundo argumento `weights`, esto espera un objeto con los nombres de los campos y el peso que tiene cada uno.

### Eliminar índice de texto combinado

A diference de los otro índices que se eliminan pasando como parámetro la misma condición con la que se creó, para eliminar un índice de texto combinado es necesario pasar como parámetro el nombre del índice entre comillas.

## Crear índice en segundo plano

Los índices se pueden agregar en primer plano o en segundo plano. Si el índice se crea en primer plano la colección se bloqueará, mientras que si se crea en segundo plano la colección seguirá estando accesible. La ventaja de crearla en primer plano es que es más rápido. Para crear un índice en segundo plano hay que pasar como segundo parámetro `{background: true}`, `db.collection.createIndex({"field": "text"}, {background: true})`. Esto es muy útil para bases de datos que se encuentren en producción.
