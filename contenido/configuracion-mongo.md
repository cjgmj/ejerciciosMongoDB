# Configuración mongo

Para conectarse al servidor se ejecutaría el comando `mongo`, esto inicia el cliente de conexión. Al ejecutar el comando para conectarse al servidor hay una serie de parámetros para modificar la configuración:

- `--help`: muestra toda la configuración disponible con descripciones de las mismas.
- `--host host`: sirve para definir el host del servidor al que se conectará. Por defecto es `localhost`.
- `--nodb`: sirve para ejecutarlio sin conectarse a una base de datos.
- `--port port`: sirve para definir el puerto del servidor al que se conectará. Por defecto es `27017`.
- `--quiet`: sirve para cambiar la forma en la que se registra o genera logs.

Además de estos comandos de configuración, una vez conectados, también hay otras opciones interesantes:

- `help`: muestra algunos comandos importantes con descripciones de los mismos.

[Volver al índice](../README.md)
