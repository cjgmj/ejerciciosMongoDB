# Datos geoespaciales

## Crear datos

Como se indica en la [documentación oficial](https://docs.mongodb.com/manual/reference/geojson/) es posible insertar distintos tipos de datos geoespaciales. Los datos deben ser insertado en un documento embebido y la estructura deberá contener el tipo (`type`) y las coordenadas (`coordinates`), siendo las coordenadas una lista.

## Búsqueda de localizaciones

Para buscar localizaciones cercanas a una ubicación hay que usar la función `$near` que requiere de `$geometry` como valor, `db.collection.find({fieldName: {$near: {$geometry: {type: "type", coordinates: [long, lat]}}}})`. Esta consulta devuelve los puntos de la colección que estén cerca del punto buscado. Además de `$geometry` se puede pasar `$maxDistance` que define la distancia máxima, en metros, a las que deben estar los puntos, y `$minDistance` la distancia mínima.

Se puede obtener todas las localizaciones cercanas dentro del radio de un punto concreto con la función `$geoWithin` que requiere `$centerSphere` como valor, `db.collection.find({fieldName: {$geoWithin: {$centerSphere: [[long, lat], km / 6378.1]}}})`. `$centerSphere` permite obtener un círculo alrededor de un punto, y espera como valor una lista de dos elementos, el primero es una lista con las coordinadas del centro del cículo en la que se buscará y el segundo es el radio del mismo, este valor debe ser indicado en radianes. El valor puede ser transformado desde kilómetros o millas. En la [documentación](https://docs.mongodb.com/manual/tutorial/calculate-distances-using-spherical-geometry-with-2d-geospatial-indexes/) se puede encontrar como hacer la transformación.

### Búsqueda de localizaciones en un área

Para buscar localizaciones dentro de un área hay que usar la función `$geoWithin` que también requiere de `$geometry` como valor, `db.collection.find({fieldName: {$geoWithin: {$geometry: {type: "Polygon", coordinates: [long, lat]}}}})`. El tipo será `Polygon` para indicar que es un área. Para cerrar el área el último punto debe ser el mismo que el primero.

### Comprobar coordenadas dentro de un área

Para comprobar si un punto está dentro de un área se usa la función `$geoIntersects` que también requiere de `$geometry` como valor, `db.collection.find({fieldName: {$geoIntersects: {$geometry: {type: "Point", coordinates: [long, lat]}}}})`. El tipo será `Point` para indicar que es un punto geográfico. Si el punto buscado está dentro de varias áreas se devolverían todas ellas. También es posible comprobar si un área se toca o está dentro de otro área.

## Crear índice geoespacial

Para crear un índice sobre datos geoespaciales hay que darle el valor de `2dsphere`, `db.collection.createIndex({fieldName: "2dsphere"})`.
