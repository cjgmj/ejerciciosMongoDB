# Seguridad

La autenticación se trata de identificar a los usuarios en la base de datos. La autorización se trata de identificar lo que los usuarios pueden hacer en la base de datos. MongoDb utiliza un sistema basado en roles, que están formados por un conjunto de privilegios. Un privilegio es una combinación de un recurso, por ejemplo una colección de la base de datos, y una acción, por ejemplo consultar la colección. Para entrar con autenticación a MongoDB en el comando de arranque hay que añadir `--auth`.

## Tipos de usuario

Dentro de una base de datos habrá distintos tipos de usuario, entre ellos pueden estar:

- Administrador: necesitará cambiar la configuración de la base de datos, crear usuarios, etc, pero como administrador no necesitará insertar o consultar datos de las colecciones.
- Desarrollador o aplicación: necesitará insertar, modificar, eliminar y consultar datos de las colecciones, pero no necesitarán poder modificar la configuración o crear usuarios.
- Científico de datos: necesitará consulta datos de las colecciones, pero no necesitará poder realizar ninguna otra acción.

## Iniciar sesión

Cuando el servidor se ejecuta con `--auth`, hay dos formas de iniciar sesión. Una forma sería con el método `auth()`, el cual espera el usuario y la contraseña como parámetros, y la otra forma sería, al conectarse a MongoDB por comandos añadir `-u user -p password --authenticationDatabase database` donde `authenticationDatabase` es la base de datos donde se creó el usuario. Una vez iniciada la sesión hay que ejecutar el comando para cambiar a la base de datos correspondiente, `use database`.

## Cerrar sesión

Si se conecta a una base de datos haciendo login y dentro de ella se conecta con otro usuario, seguiré estando conectado con el usuario antiguo. Antes de conectar con el nuevo usuario habrá que ejecutar el método `logout()` para cerrar la sesión anterior. El cierre de sesión se debe hacer mientras está en la base de datos sobre la que se creó el usuario.

## Crear usuario

Los usuarios se crean con la función `createUser()`. Los usuarios se crean para una base de datos, estos además del usuario y la contraseña tendrán un rol o varios de ellos. El acceso a la base de datos no está limitado a la autenticación. Esta función espera un documento que debe contener el usuario, la contraseña y los roles, `{user: "user", pwd: "password", roles: ["role"]}`. Hay una serie de roles predefinidos por MongoDB que se pueden encontrar en la [documentación](https://docs.mongodb.com/manual/reference/built-in-roles/).

## Modificar usuario

Los usuarios se modifican con la función `updateUser()`. Este método permite, por ejemplo, cambiar la contraseña del usuario o añadir/quitar roles. Este método espera como primer parámetro el nombre del usuario y como segundo parámetro un objeto que contendrá todos los cambios a realizar en el usuario, los datos introducidos sobrescribirán a los que están guardados. Es posible dar un rol a otra base de datos pasando dentro de la lista de roles un objeto que contenga el rol y la base de datos, `{roles: [{role: "role", db: "db"}]}`.

## Ver usuario

Se puede ver la configuración de un usuario con la función `getUser()` que espera por parámetros el nombre del usuario.

## Securizar conexión

MongoDb utiliza TLS/SSL para cifrar los datos mientras se envían al servidor, los cuales serán desencriptados en el servidor con un par de claves privadas. Podemos encontrar el proceso para securizar la conexión en la [documentación](https://docs.mongodb.com/manual/tutorial/configure-ssl/).
