# Valores numéricos

Existen varios tipos numéricos: Integer (int32), Long (int64), Double (64 bit) y High Precision Dobule (128bit). La consola de MongoDB inserta por defecto los números con el tipo de Double, pero al usar un driver de MongoDB esto varía dependiendo del lenguaje, por ejemplo, en Node.js se insertaría como un Double, pero en Python lo haría como un Integer. Si un número entero se suma a uno decimal, el tipo del número será cambiado a decimal.

## Integer

Acepta números enteros. Los números deben estar entre -2.147.483.648 y 2.147.483.647. Se usa cuando se requiere guardar un número entero. Para insertar un valor como `Integer` debe estar contenido en `NumberInt(value)`. El valor puede estar contenido entre comillas o estar en plano, aunque es recomendable usar las comillas.

## Long

Acepta números enteros. Los números deben estar entre -9.223.372.036.854.775.808 y 9.223.372.036.854.775.807. Se usa cuando se requiere guardar un número entero pero sobrepasa el rango de los Integers. Para insertar un valor como `Long` debe estar contenido en `NumberLong(value)`. El valor debe ir entre comillas si no lo interpretará como un `Double` que se pasa como argumento a `NumberLong` y puede salirse del rango.

## Double

Es el valor por defecto para los números. Acepta números con decimales. Los valores decimales son aproximados. Se usa cuando se requiere guardar un número decimal y no se requiere mucha precisión en los decimales.

## High Precision Dobule

Acepta números con decimales. Los valores pueden contener hasta 34 decimales. Acepta más decimales porque tiene menos precisión en la parte entera. Se usa cuando se requiere guardar un número decimal con alta precisión en los decimales. Es el tipo correcto para valores con los que se harán operaciones matemáticas desde MongoDB. Para insertar de este tipo debe estar contenido en `NumberDecimal(value)`. El valor puede estar contenido entre comillas o estar en plano, aunque es recomendable usar las comillas.
