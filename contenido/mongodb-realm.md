# MongoDB Realm

Es un servicio ofrecido por MongoDB para poder crear aplicaciones serverless. Tiene integración con Cloud Database (Atlas) y además provee autenticación para la aplicación. Permite también realizar acciones cuando se ejecutan eventos dentro de Stitch, por ejemplo enviar un correo cuando se inserta un registro en una colección. También permite ejecutar código o algún tipo de funcionalidad en la nube.
