# Configuración mongod

Se puede iniciar un servidor de mongo ejecutando el comando `mongod` en la terminal de Windows. Al ejecutar el comando para crear el servidor hay una serie de parámetros para modificar la configuración:

- `--config`: sirve para indicar el fichero en el cual se guarda la configuración de inicio del servidor. La forma abreviada sería `-f`. El archivo puede tener la extensión `conf` ó `cfg`.
- `--dbpath path`: sirve para cambiar la ruta en la que se guardan las bases de datos.
- `--directoryperdb`: sirve para no tener todos los archivos de colección almacenados en el directorio raíz, si no que se guardarían en subcarpetas por base de datos.
- `--fork`: esta configuración solo se ejecuta en Mac y Linux y tiene que ser usada junto con `--logpath` o `--syslog`. Sirve para arrancar el servidor como un proceso en segundo plano. En Windows esta opción está disponible si se instala como servicio, para hacer uso de la misma debe iniciar el `cmd` como administrador ejecutar `net start mongodb`, de esta forma se iniciaría como un proceso en segundo plano. Para parar el servidor se ejecuta `use admin` y `db.shutdownServer()`. Además de la anterior forma, en Windows se puede parar ejecutando `net stop mongodb`.
- `--logpath path`: sirve para cambiar la ruta del archivo en el que se guardan los logs.
- `--help`: muestra toda la configuración disponible con descripciones de las mismas.
- `--port port`: sirve para cambiar el puerto predeterminado. Por defecto es `27017`.
- `--quiet`: sirve para cambiar la forma en la que el servidor registra o genera logs.
- `--repair`: sirve para reparar el servidor si tiene algún problema de conexión y hay advertencias o errores relacionados con la corrupción de los archivos de la base de datos.
- `--storageEngine arg`: sirve para cambiar el motor de almacenamiento. El motor por defecto es `wiredTiger`.

[Volver al índice](../README.md)
