# Esquemas y relaciones

## Esquemas

MongoDB no aplica ningún esquema. Los documentos dentro de una misma colección pueden tener distintos esquemas. Aunque todos los documentos de una misma colección puedan tener distintos esquemas, posiblemente no sea lo que se necesite en el desarrollo de una aplicación. Se puede decir que hay tres formas de definir una colección, en la que todos los documentos tienen un esquema diferente, en la que los documentos tienen un esquema definido pero algunos de ellos pueden tener datos extras y en la que todos los documentos tienen el mismo esquema. Aunque el enfoque normalmente se encuentra entre los dos últimos descritos, no existe uno mejor que otro por lo que se puede elegir el enfoque que más convenga para lo que se quiere realizar.

## Tipos de datos

Se pueden consultar todos los tipos en este [enlace](https://docs.mongodb.com/manual/reference/bson-types/).
Se pueden consultar todas las restricciones en este [enlace](https://docs.mongodb.com/manual/reference/limits/).
Se pueden consultar los tipos de la API para Node.js (versión 3.6) en este [enlace](https://mongodb.github.io/node-mongodb-native/3.6/api/index.html).

- `Text`: este tipo de datos debe ir con comillas dobles (`""`) o comillas simples (`''`). No afecta como se introduzca directamente en el `shell` pero puede afectar dependiendo del lenguaje que se esté usando. Como máximo puede tener un tamaño total de 16MB.
- `Boolean`: son valores simples, `true` o `false`.
- `Number`: existen varios tipos para los valores numéricos. Para insertar un tipo específico hay que llamarlo como método pasándole por parámetros el valor.
  - `Integer (int32)`: tienen 32 bits de largo. Si sobrepasa ese rango se convertirá en un número diferente. Valor máximo `+-2,147,483,647`.
  - `NumberLong (int64)`: tienen 64 bits de largo. Valor máximo `+-9,223,372,036,854,775,807`.
  - `NumberDecimal`: representa números decimales con alta precisión. Puede almacenar hasta 34 decimales.
- `ObjectId`: es generado automáticamente por MongoDB para dar una identificación única a un documento.
- `ISODate`: representa una fecha.
- `Timestamp`: principalmente se usa internamente. Representa una fecha de creación única, la cual es creada a partir de la fecha actual junto con un valor ordinal.
- `Embedded Document`: almacena documentos embebidos.
- `Array`: almacena listas de valores.

### Notas

- Si se inserta un valor númerico por el `shell` se trata como un decimal de 64 bits y no como uno de los objetos descritos arriba. Si se necesita almacenar números muy grandes, se deben de almacenar de otro modo, por ejemplo como un `String`.
- Se puede guardar la fecha actual con `new Date()`, en el objeto aparecerá como `ISODate`, y un timestamp con `new Timestamp()`, en el objeto aparecerá como `Timestamp`.

## Modelar datos

Para modelar datos es importante:

- Saber que tipo de datos va a necesitar o generar la aplicación, y definir los campos que se necesitan.
- Saber dónde se van a usar los datos y definir las colecciones requeridas, como se relacionan entre ellas y los campos de los documentos de cada colección.
- Saber que tipo de datos o información se quiere mostrar en la aplicación y definir las queries que se necesitan.
- Saber con que frecuencia se obtendrán datos y optimizar la forma en la que se tratan las relaciones. Es posible duplicar datos para mejorar las relaciones.
- Saber con que frecuencia se insertan o modifican datos y definir como insertar los datos. No se pueden duplicar para mejorar relaciones si estos datos se modifican con frecuencia, ya que habría que actualizarlos en todas las relaciones.

## Relaciones

Las relaciones entre colecciones pueden ser mediante:

- Documentos embebidos: son datos que están muy relacionados con la colección y que no sufren muchas modificaciones. Posiblemente estos datos siempre se devuelvan al consultar el documento o al menos se quiera tener una forma rápida de obtenerlos.
- Referencias: son datos guardados en otra colección y que, posiblemente, se repitan en más de un documento. El campo dentro del documento tendrá el valor del id al que hace referencia.

## Fusionar documentos

Cuando se tienen documentos relacionados por referencias se pueden obtener ambos documentos combinados con una sola consulta. Para incluir los datos de los documentos relacionados hay que añadir lo siguiente en la consulta de la colección:

```MongoDB
aggregate([{$lookup: {
  from: "collectionRelated",
  localField: "fieldName",
  foreignField: "fieldReferenced",
  as: "fieldResult"
}}])
```

De este modo devuelve tanto el campo que almacena la referencia como los objetos referenciados.

## Validación del esquema

Dentro de una colección se puede definir un validador de esquema que permite indicar los documentos que se validarán y que ocurrirá si la validación falla. Hay distintas formas de definir el nivel de validación:

- `strict`: valida todas las inserciones y las modificaciones.
- `moderate`: valida todas las inserciones pero solo se validan las modificaciones sobre documentos válidos. Si había documentos antes de configurar el esquema y no cumplen con la validación podrán ser modificados sin que se validen.

Si la validación falla puede mostrarse:

- `error`: lanza un error y no se guarda o modifica el documento.
- `warn`: registra un mensaje de advertencia en el archivo de logs pero se realiza la inserción o la modificación.

Se puede consultar todas las opciones en la [documentación](https://docs.mongodb.com/manual/core/schema-validation/).

Para crear la validación de una colección se tiene que usar el comando `createCollection()`. Por ejemplo:

```MongoDB
db.createCollection("collectionName", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["requiredFieldNames"],
      properties: {
        fieldName: {
          bsonType: "fieldType",
          description: "fieldDescription"
        },
        arrayField: {
          bsonType: "array",
          description: "fieldDescription",
          items: {
            bsonType: "object",
            required: ["requiredFieldNames"],
            properties: {
              fieldName: {
                bsonType: "fieldType",
                description: "fieldDescription"
              }
            }
          }
        }
      }
    }
  }
})
```

`bsonType: "object"` indica que todo lo que se agregue a la colección debe ser un documento.
El valor `bsonType` para los campos que guardan el identificador de otros documentos debe ser `objectId`.
El `bsonType` de un array no siempre será `object`.

Si se introducen campos adicionales a los requeridos pasará la validación. Estos campos adicionales podrán ser validados si se añaden a `properties`.

Para cambiar la validación de un documento hay que ejecutar `db.runCommand({collMod: "collectionName", newValidator})`. Por ejemplo:

```MongoDB
db.runCommand({collMod: "collectionName", validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["requiredFieldNames"],
      properties: {
        fieldName: {
          bsonType: "fieldType",
          description: "fieldDescription"
        },
        arrayField: {
          bsonType: "array",
          description: "fieldDescription",
          items: {
            bsonType: "object",
            required: ["requiredFieldNames"],
            properties: {
              fieldName: {
                bsonType: "fieldType",
                description: "fieldDescription"
              }
            }
          }
        }
      }
    }
  },
  validationAction: "error|warn",
  validationLevel: "strict|moderate"
})
```

`validationAction` por defecto es `error`.
`validationLevel` por defecto es `strict`.
