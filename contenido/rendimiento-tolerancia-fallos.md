# Rendimiento y tolerancia a fallos

## Colecciones limitadas

Las colecciones limitadas son un tipo especial de colección, que se deben crear explícitamente, donde se limita la cantidad de datos o documentos que se puede almacenar. Cuando se exceda el límite indicado, los documentos más antiguos serán eliminados. Para crear una colección de este tipo, se usa la función `createCollection()` pasando dentro de las opciones `capped:true`. Es obligatorio añadir en la cofiguración `size`, si el campo tiene un valor inferior o igual a 4096 bytes, la colección será limitada a 4096 bytes, en caso contrario, se aumentará el valor introducido para hacerlo múltiplo de 256. Además es posible pasar `max`, el cual es un valor opcional, para definir la cantidad máxima de documentos que se puede almacenar, `db.createCollection("collectionName", {capped: true, size: size, max: maxDocuments})`. El orden en el que se devuelven los documentos en una consulta es el orden en el que se insertaron. Se puede invertir el orden añadiendo `sort({$natural: -1})`.

## Conjuntos de réplicas

Los conjuntos de réplicas son un conjunto de nodos secundario que se conectan de forma asíncrona con el nodo primario. Esto se hace por si el nodo primario se desconecta, se pueda conectar a uno de los nodos secundarios, el cual será elegido como nodo primario, contra el que se harán todas las conexiones hasta que se restablezca todo el conjunto de réplica. Las peticiones de escritura siempre van al nodo primario, pero las peticiones de lectura pueden ir, si el servidor está configurado correctamente, a los nodos secundarios. De esta forma se aligera las conexiones de lectura, al no depender solo de un nodo, y mejora la tolerancia a fallos.

## Fragmentación

La fragmentación consite en dividir los datos de la base de datos en distintos servidores (escalado horizontal). Para esto MongoDB ofrece un intermediario (`mongos`) el cual es un entrutador. `mongos` es responsable de reenviar sus operaciones a los fragmentos correctos. Para la división utilizará una `shard key`, que es un campo que se agrega a cada documento para que el servidor sepa a cual de los fragmentos pertenece. Si no tienen `shard key`, `mongos` enviará la solicitud a todos los fragmentos y cada uno de ellos debe verificar si es el responsable de esos datos y enviar la respuesta, una vez enviada todas las respuesta, `mongos` fusiona todos los datos y los devuelve.
