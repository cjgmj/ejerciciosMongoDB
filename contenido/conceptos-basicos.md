# Conceptos básicos

## Base de datos, colecciones y documentos

Cada base de datos puede contener una o más colecciones. Las colecciones son el equivalente a las tablas de una base de datos SQL. Cada colección contiene documentos, que es la información con la que se trabajará.

Las bases de datos, las colecciones y los documentos se crean automáticamente cuando se comienza a almacenar datos.

## Crear base de datos y colecciones

En el shell se puede ejecutar `show dbs` para mostrar las bases de datos que tiene el servidor en el que se está conectado. Para cambiar la conexión a otra base de datos hay que ejecutar el comando `use nameDb`. Se puede cambiar a una base de datos que no existe para crearla. Aunque se cambie a una base de datos que aún no existe, si se ejecuta el comando de mostrar las bases de datos esta no aparecerá. Esto ocurre porque la base de datos no se crea hasta que comienza a guardar datos. Se hace referencia a la base de datos a la que se está conectado con `db`. Se puede mostrar las colecciones de una base de datos con `show collections`.

Para insertar datos hay que hacer referencia a la colección mediante `db.nameCollection`, si esta no está creada se creará al guardar documentos en ella. Como estándar los nombres de las colecciones van en plural. Para insertar un documento se ejecuta `db.nameCollection.insertOne(document)`, este siempre se define con llaves (`{}`) y tienen formato JSON. Los documentos pueden tener distintos esquemas dentro de una misma colección. Los documentos están formados por pares de clave-valor, las claves será los nombres, deben estar encerrados entre comillas dobles (`""`), las comillas se pueden omitir siempre que no haya espacios en blanco, y el valor de las claves, pueden ser de diferentes tipos de datos, como por ejemplo string, number o boolean, los de tipo number y boolean no van encerrados en comillas dobles como si pasa con los tipos string. Al crearse el documento, MongoDB le proporciona un identificador único (`_id`) de tipo `ObjectId`, el cual es devuelvo al crearse el documento. Se puede añadir un id en la creación introduciendo el campo `_id`, el cual tiene que ser único, al documento. Estos identificadores no serán del tipo `ObjectId` sino que serán del tipo introducido.

Para realizar búsquedas en una colección hay que ejecutar el comando `db.nameCollection.find()`, de este modo mostrará todos los documentos de la colección. Se pueden mostrar los documentos de una forma más legible añadiendo la función `pretty`, `db.nameCollection.find().pretty()`.

## Operaciones CRUD

### Create

- `insertOne(data, options)`: permite insertar un documento en una colección.
- `insertMany(data, options)`: permite insertar varios documentos en una colección. Los datos deben ser pasados como un array. Si en la lista a insertar hay algún documento que provoca error se cancela la operación pero no revierte los documentos ya insertados, a esto se le denomina inserción ordenada y es el comportamiento por defecto. Se puede evitar este comportamiento pasando como segundo argumento `{ordered: false}`, de esta forma inserta los documentos y en la respuesta muestra una lista con los documentos que han fallado con el mensaje de error correspondiente.
- `insert(data, options)`: permite insertar tanto un único documento como varios documentos en una colección. No es recomendable usar este método. El método no devuelve el id en la respuesta.
- `mongoimport`: permite importar documentos. Desde el terminal, hay que navegar hasta la carpeta donde se encuentra el fichero json que se quiere importar y ejecutar el comando `mongoimport nombreArchivo -d nombreBasedatos -c nombreColección --jsonArray --drop`, si el sistema no reconoce el comando `mongoimport` se debe navegar a la carpeta bin dentro de la instalación de mongo. `--jsonArray` sirve para indicar que en el archivo hay más de un documento a importar. `--drop` indica que si esa colección existe se eliminará antes de importar los nuevos documentos, si no se añade, los nuevos documentos se añadiran a los existentes.

`writeConcern`: para mayor seguridad como segundo parámetro se puede pasar `{writeConcern: {w: 1, j: true, wtimeout: 200}}`. `w: 1`, valor por defecto, sirve para esperar que el servidor confirme la inserción y enviar el la respuesta el id del documento, si se configura como `w: 0` devolverá la respuesta antes de que el servidor inserte el objeto por lo que con la respuesta no se sabrá si fue insertado o no. `j: true` sirve para añadir las operaciones a un archivo que sirve de backup en caso de perdida de conexión, el valor por defecto es `false` o `undefined`, al ponerlo como `true`, la inserción será más lenta ya que esperará a que termine la edición del archivo. `wtimeout: 200` sirve para añadir un timeout a la escritura, en caso de que la escritura tarde más tiempo del indicado devolverá un error.

**Nota:** Cuando se quiere indicar que un campo no tiene valor se iguala a `null`.

### Read

- `find(filter, options)`: permite devolver todos los documentos, si no se le pasan filtros o se pasa como filtro `{}`, o una lista filtrada de documentos de una colección. Se pueden mostrar los documentos de una forma más legible añadiendo la función `pretty()`, la cual solo funciona con los `Cursor Object`. La función `find` devuelve el `Cursor Object` de los primeros 20 documentos.
- `findOne(filter, options)`: permite devolver el primer documento de una colección que coincida con el filtro aplicado, o el primero de la colección si no se le añade ningún filtro. No soporta la función `pretty`. Podemos acceder a un campo del documento añadiendo tras la función un punto seguido del nombre del campo, `findOne({}).nombreCampo`. Si la colección está vacía devolverá `null`.

Los filtros se forman a partir de un objeto con clave-valor, por ejemplo `{age: 32}`, esto permite filtrar los resultados de la consulta. Se puede añadir más de una condición separando con comas los pares de clave-valor, siempre que la clave no se repita, ya que la segunda clave sobrescribirá el filtro de la primera clave, en ese caso se debe usar `$and`. También es posible añadir operaciones a los filtros, como por ejemplo `{age: {$gt: 30}}`, se pueden reconocer las operaciones porque siempre comienzan con `$`. Se puede filtrar un campo por varias operaciones separándolas por comas. Para consultar campos embebidos dentro de otros campos se pueden concatenar con `.`, por ejemplo `address.street`. Si un campo es un array de documentos, para consultar por un valor específico dentro de los documentos se puede hacer como si fuera un documento plano en vez de un array. Para obtener los documentos que en un campo de tipo array contenga un valor la consulta se haría un filtro básico, no hace falta operador. Si se quiere filtrar para que solo contenga en valor indicado, el valor deberá ir entre corchetes (`[]`). Es posible saber si un documento tiene un campo con el operador `$exists`.

Existen dos tipos de operadores relacionados con la lectura, los [operadores de consulta](https://docs.mongodb.com/manual/reference/operator/query/#query-selectors) permiten reducir el conjunto de documentos que se recuperan y los [operadores de proyección](https://docs.mongodb.com/manual/reference/operator/query/#projection-operators) que permiten transformar o cambiar los datos devueltos. Los operadores de consulta se dividen en las siguientes categorías: comparación, lógicos, elementos, evaluación, array, comentarios y geoespaciales, estas últimas permiten trabajar con datos de ubicación, por ejemplo, para calcular que lugar está más cerca de unas coordenadas específicas. También existen los [modificadores de consulta](https://docs.mongodb.com/manual/reference/operator/query-modifier/) permiten cambiar el comportamiento de la consulta pero se encuentran en desuso.

#### Cursor

Al hacer la consulta con un `find` se devuelve un cursor. Básicamente, un cursor es un puntero que tiene almacenada la consulta realizada. Esto permite pedir los siguientes documentos que deben ser devueltos por la consulta de forma rápida. Por defecto, en la terminal devuelve documentos de 20 en 20, si se usa el driver de mongo desde una aplicación, el cursor se debe controlar manualmente. Si se quieren obtener los siguientes 20 documentos, una vez obtenido el resultado, habrá que ejecutar el comando `it`. Se puede guardar el cursor devuelto al ejecutar la función `find()` en una variable, lo cual es posible porque la `shell` está basada en JavaScript, e ir obteniendo los documentos en orden ejecutando la función `next()`.

Es posible obtener el total de documentos devueltos en una consulta con el método `count()`. Es posible formatear el resultado para mostrarlo de una forma más legible con el método `pretty()`.

Es posible devolver una lista de todos los documentos en un array con la función `toArray()`. También se pueden imprimir todos los documentos de un cursor con el método `cursor.forEach((documet) => {printjson(document)})`, el método `printjson` es un comando propio de la `shell`. Si tras usar esta función se ejecuta la función `next()` devolverá un error al haberse recorrido el cursor por completo. Se puede comprobar si el cursor se ha recorrido por completo con la función `hasNext()`.

Se puede ordernar los resultados devueltos por el cursor con el método `sort()`, debe ir siempre tras el método `find()`. El método `sort()` espera un objeto en el cual se describe como debe ordenarse el resultado. Se pueden ordenar por campos de documentos embebidos concatenándolos con `.`. El orden se indica con `1` para orden ascendente y `-1` para orden descendente. Se pueden ordenar por varios campos separándolos por comas dentro del objeto.

Es posible omitir cierta cantidad de documentos con el método `skip()`. Esto es útil, por ejemplo, para paginación. Como parámetro recibe el número de documentos que serán omitidos. También es posible limitar la cantidad de documentos devueltos en la consulta con el método `limit()`, el cual recibe como parámetro el número máximo de documentos que se devolverán.

Si se usan los métodos `sort()`, `skip()` y `limit()` en la misma consulta, da igual en el orden en el que se usen, ya que el cursor automáticamente lo hará en el orden correcto, el cual es ordenar, omitir y limitar.

#### Proyección

La proyección permite controlar los datos que serán devueltos en una consulta. La proyección se pasa al método `find()` a través del parámetro `options`, si la consulta no tiene filtros el primer parámetro será `{}`. Para indicar los campos que serán devueltos, se pasará el nombre del campo y `1`, por ejemplo `{name: 1}`. En caso de que se quiera devolver varios campos irán separados por comas. Los campos que no se incluyan se pasarán o que se agreguen con `0` no serán devueltos. El `_id` siempre será incluido en el resultado si no se agrega explícitamente con `0`. Se pueden obtener campos embebidos concatenándolos con `.` para así no devolver el documento embebido completo y solo los campos especificados.

También es posible usar la proyección con arrays. Cuando se filtran los documentos que contengan un valor dentro de un campo y este sea de tipo array se puede añadir en `options` `"fieldName.$": 1`, este devolvería el campo del filtro con el valor que se está filtrando y el `_id` del documento, si no se especifica para que no sea devuelto. Si en el filtro se le pasan varios filtros dentro del array, en el resultado solo se mostrará la primera que encuentre. Se pueden pasar también operadores para obtener si se debe devolver el campo o no, por ejemplo con el operador `$elemMatch`. También es posible usar el operador `$slice`, el cual como primer parámetro recibe el número de elementos que se omitirán y como segundo parámetro recibe el número de elementos que serán incluidos en la respuesta.

Los filtros no siempre estarán relacionados con la proyección, se pueden filtrar por unos campos para devolver otros campos de los documentos que cumplan el filtro.

### Update

- `update(filter, data, options)`: permite modificar varios documentos de una colección.
- `updateOne(filter, data, options)`: permite modificar un documento de una colección. `filter` restringe el documento que se cambiará y `data` los datos por los que se modificarán.
- `updateMany(filter, data, options)`: permite modificar varios documentos de una colección. `filter` restringe los documentos que se cambiarán y `data` los datos por los que se modificarán. Si se pasa como filtro `{}` se modificarán todos los documentos de la colección.
- `replaceOne(filter, data, options)`: permite reemplazar un documento existente de una colección por uno nuevo.

El parámetro `filter` admite todos los filtros que se pueden pasar para hacer consulta. Los operadores que pueden ser utilizados para la modificación se pueden ver [aquí](https://docs.mongodb.com/manual/reference/operator/update/). Se pueden pasar varias modificaciones dentro del objeto de `data` separados por coma. Si los datos se pasan con `{$set: objeto}` se modificará o agregará el campo dentro del documento existente, si no se pasan con el operador `$set` el documento será sobrescrito. Para cambiar el valor numérico de un campo se puede usar el operador `$inc`, pasando el nombre del campo y el valor en el que se incrementará, por ejemplo `$inc: {age: 1}`. Si se intenta aplicar dos o más operaciones sobre el mismo campo dará conflictos.

El operador `$min` solo cambia el valor si el nuevo valor es inferior al existente. El operador `$max` solo cambia el valor si el nuevo valor es superor al existente. El operador `$mul` permite multiplicar el valor del campo por el indicado.

Es posible eliminar campos de un documento con el operador `$unset`, el valor que se le pasa junto al nombre del campo suele ser `""` ya que lo importante para este operador es el nombre del campo. Se puede renombrar el nombre de un campo del documento con el operador `$rename`, el valor que se le pasa junto al nombre del campo será el nuevo nombre del campo.

Si se quiere insertar o modificar un documento pero no se sabe si se encuentra ya en la colección o no, se puede usar el método `upsert()` que se pasa dentro de `options` como `{upsert: true}`, el valor por defecto es false. La respuesta con esta opción será el total de documentos que coinciden con el filtro, el total de documentos modificados y el id de los documentos insertados.

La respuesta al ejecutar estos métodos son el total de documentos que coinciden con el filtro y el total de documentos modificados. El total de documentos modificados puede variar al total de documentos que coinciden con el filtro porque puede que no tenga sentido aplicar los cambios sobre un documento, por ejemplo cuando el valor a modificar coincide con el valor actual del campo.

**Nota:** Los datos se tienen que añadir como `{$set: objeto}`, donde el objeto sería la serie de campos que se quieran añadir o sustituir. El signo de dólar (`$`) en MongoDB es una palabra reservada.

#### Array

Se puede modificar el valor de un elemento de un array con el signo `$`, esto indica que se modificará el elemento que coincida con el filtro, por ejemplo `{$set: {"fieldName.$": newValue}}` Para crear un nuevo campo dentro del elemento del array que coincida con el filtro se concatenería con un punto tras el `$`, `fieldName.$.newField`. El `$` solo hace referencia a la primera coincidencia del filtro, en caso de querer cambiar todas las coincidencias del filtro hay que usar `$[]`. Dentro de `[]` se puede establecer un identificador para usarlo junto con `arrayFilters` que se parará dentro de `options`, y permitirá definir algunas condiciones por las que se filtrarán los elementos, estas condiciones pueden ser diferentes a las del filtro, por ejemplo `db.col.updateMany({}, {$set: {"fieldName.$[el].fieldName": fieldValue}, {arrayFilters: [{"el.fieldName": {filter}}]}})`.

Se puede añadir un nuevo elementos a un array con el operador `$push`, si se quiere añadir más de uno, tras el nombre del campo se debe usar el operador `$each` que espera una lista con los nuevos elementos a insertar. Al operador `$each`, también es posible pasarle el operador `$sort` para indicar el orden en el cual deben insertarse los nuevos elementos, ordena también los registros existentes dentro del array. Para añadir un elemento a una lista, si este no se encuentra en la misma se usa el operador `$addToSet`.

Se pueden eliminar elementos a un array con el operador `$pull` para eliminar un elementos específico, para eliminar el último elemento de la lista se usa el operador `$pop` junto con el valor `1` y con `-1` para eliminar el primer elemento de la lista.

### Delete

- `deleteOne(filter, options)`: permite eliminar un documento de una colección.
- `deleteMany(filter, options)`: permite eliminar varios documentos de una colección. Si se pasa como filtro `{}` se eliminarán todos los documentos de la colección.
- `drop()`: permite eliminar todos los documentos de una colección.
- `dropDatabase()`: permite eliminar la base de datos actual.

## Projection

Sirve para filtrar los campos de los documentos devueltos, evitando así transferir datos innecesarios. Para filtrar los campos, en el `find` se tienen que pasar como segundo parámetro (`options`) pares de valores con los nombres de los campos junto con `0` o `1` dependiendo si se quiere recibir o no, por defecto los campos no son incluidos solo habría que indicar los que se quieren devolver. El `_id` por defecto siempre es devuelto, se debe indicar explícitamente si no se quiere devolver. Un ejemplo de esto sería `db.nameCollection.find({}, {name: 1, _id: 0}).pretty()`.

## Embedded Documents

Los documentos pueden tener como valor de un campo otro documento. El valor debe ir entre llaves (`{}`). Hay dos límites a la hora de embeber documentos, estos no pueden superar los 100 niveles de anidamiento y el tamaño total del documento debe ser inferior a 16MB. Se pueden filtrar los campos de los documentos embebidos concatenando los campos con un punto (`.`), esto debe ir entre comillas dobles (`""`) de lo contrario dará error, junto con el valor que debe tener. Se pueden concatenar tantos campos como sea necesario.

## Arrays

Los documentos pueden tener como valor de un campo una lista. El valor debe ir entre corchetes (`[]`). Las listas pueden estar formadas por cualquier tipo de dato que pueda tener un documento. Se pueden filtrar por los valores de la lista pasando a `find` el nombre del campo junto con el valor que debe contener.

## Eliminar base de datos y colecciones

Para eliminar una base de datos simplemente hay que conectarse a ella con `use nameDb` y ejecutar `db.dropDatabase()`. Para eliminar una colección de una base de datos hay que usar `db.nameCollection.drop()`.

[Volver al índice](../README.md)
