# Comandos básicos

- Se puede cambiar el puerto en el que se inicia MongoDB con el comando `mongod --port puerto`. Si se cambia el puerto, para conectar a la base de datos hay que ejecutar `mongo --port puerto`.
- Para limpiar el contenido de la consola ejecutar `cls`.
- La terminal autocompleta palabras al pulsar `TAB`.
- Para salir de MongoDB ejecutar `exit`.
- Ver base de datos: `show dbs`.
- Ver colecciones: `show collections`.
- Conectar a una base de datos (si no existe la base de datos, se creará): `use nameDb`.
- Ver estadísticas de la base de datos: `db.stats()`.
- Comprobar tipo de un valor almacenado: `typeof db.collection.findOne().campo`
- Insertar datos (una vez conectado a una base de datos, si la colección no existe se creará): `db.collection.insertOne({"name":"A Book", price: 12.99})`. Al no tener un esquema definido se pueden introducir distintos tipos de objetos `db.collection.insertOne({"name": "Another book", "price": 19.99, "description": "A must read"})`.
- Ver datos de una colección: `db.collection.find()`. Se pueden ver los datos formateados añadiendo pretty: `db.collection.find().pretty()`.
- Se puede borrar una base de datos conectándose a la misma y ejecutando `db.dropDatabase()`.
- Se puede borrar una colección ejecutando `db.collection.drop()`.
- Se puede ejecutar un archivo JavaScript desde la consola con `mongo fileName.js`, de esta forma mongo se conecta al servidor y ejecuta el fichero JavaScript dentro del servidor.

[Volver al índice](../README.md)
