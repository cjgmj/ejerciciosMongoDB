# Agregaciones

Es un método de búsqueda alternativa que permite ejecutar una serie de pasos sobre los datos que se recuperan de la colección para devolverlos de la forma que se necesita. Para hacer uso de la agregación usaremos el método `aggregate`, el cual espera una lista como parámetro. Esta lista contendrá la serie de pasos que se debe ejecutar sobre los datos, cada uno de ellos será un documento. Los índices también afectan a la búsqueda por agregación, e igual que con `find` también se devuelve un cursor. El primer paso será el filtrado, para ello se usa `$match` y recibe los mismo filtros que el `find()`. Un ejemplo de agregación sería `db.collection.aggregate([{$match: {field: "value"}}])`.

## Agrupar datos

Para agrupar datos en la consulta se usa `$group`, el cual espera un documento que puede contener varios parámetros. El primer parámetro es `_id` que define por el campo que se van a agrupar, este valor será un documento que tendrá el nombre del nuevo campo resultante en la agrupación y como valor `$` seguido de los campos por los que se agruparán. El segundo parámetro recibirá otro documento con el tipo de agregación que se desea ejecutar, se pueden ver las funciones disponible en la [documentación](https://docs.mongodb.com/manual/reference/operator/aggregation/group/). Un ejemplo para contar los documentos una vez agrupados sería `db.collection.aggregate([{$group: {_id: {fieldGroupName: "$field"}, newField: {$sum: 1}}}])`.

Si se quiere ordenar el resultado una vez agrupado, se tendrá que añadir una nueva función `$sort` la cual espera un documento con el nombre del campo devuelto en la agrupación y la dirección de la ordenación.

## Transformar documentos

Para transformar los documentos se usa `$project`, este funciona como la proyección del find, se indica los campos que se quieren o no obtener. También nos permite obtener valores de documentos embebidos, sin obtener todos los campos del mismo, creando campos nuevos para el resultado, esto se haría con `$concat`. `$concat` permite concatenar cadenas de texto además del valor de los campos, los cuales tienen que ir precedidos de `$`. Además se pueden usar modificadores sobre los valores de los campos, como puede ser `$toUpper`. Se pueden ver las funciones disponible en la [documentación](https://docs.mongodb.com/manual/reference/operator/aggregation/project/). Se puede capitalizar palabras de la siguiente forma:

```MongoDB
db.collection.aggregate([
    {$project: {
        _id: 0,
        capitalize: {
            $concat: [
                {$toUpper: {$substrCP: ["field", 0, 1]}},
                {$substrCP: ["field", 1, {$subtract: [{$strLenCP: "field"}, 1]}]}
            ]
        }
     }}
])
```

Es posible convertir valor mediante el operador `$convert`, este operador espera un objeto siendo el primer campo `input` que tendrá como valor el campo al que hace referencia, y como segundo campo `to` que indica a que tipo se debe convertir. Se pueden ver los tipos a los que se pueden convertir en la [documentación](https://docs.mongodb.com/manual/reference/operator/aggregation/convert/). Además se puede especificar unos campos, `onError` y `onNull`, para dar un valor por defecto si falla la conversión.

## Transformar listas

Si el documento tiene una lista de valores en uno de sus campos, en `$group` se puede usar el operador `$push` para crear una lista con todas las listas de los elementos por los que se agrupen. Para extraer los valores de los elementos del array se usa el operador `$unwind`, este operador aplana la lista duplicando el resto de datos del documento, es decir, crea un nuevo documento por cada uno de los valores de la lista. Si en la lista resultante no se quieren tener valores duplicados, en vez de usar `$push` se tendrá de usar `$addToSet`. Un ejemplo de transformación sería:

```MongoDB
db.collection.aggregate([
    {$unwind: "field"},
    {$group: {_id: {fieldGroupName: "$field"}, newField: {$addToSet: "$field"}}}
])
```

Mediante el uso de `$project` se puede obtener, por ejemplo, el primer valor de la lista con el operador `$slice`. Este operador espera una lista que debe tener en la primera posición el campo con el que se va a trabajar. Es posible obtener el tamaño de la lista con el operador `$size`.
