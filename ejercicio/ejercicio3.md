# Ejercicio 3

1. Insertar compañías.

   ```MongoDB
   db.companies.insertOne({name: "Supermoon", employees: 100, _id: 1})
   db.companies.insertMany([{
       name: "Night",
       employees: 50,
       _id: 2
   }, {
       name: "Carrefive",
       employees: 75,
       _id: 3
   }])
   ```

2. Insertar id duplicado con inserción desordenada.

   ```MongoDB
   db.companies.insertMany([{
       name: "Mercahome",
       employees: 5,
       _id: 1
   }, {
       name: "ChessPizza",
       employees: 15,
       _id: 4
   }], {
       ordered: false
   })
   ```

3. Insertar una nueva compañía con y sin writeConcern.

   ```MongoDB
   db.companies.insertOne({name: "McBurguer", employees: 10, _id: 5}, {writeConcern: {w: 1, j: false}})
   db.companies.insertOne({name: "Burguer prince", employees: 10, _id: 6}, {writeConcern: {w: 1, j: true}})
   ```
