# Ejercicio 1

1. Insertar tres pacientes con al menos una entrada en el historial por paciente.

   ```MongoDB
   db.patients.insertMany([{
       "firtName": "John",
       "lastName": "Doe",
       "age": 33,
       "history": [
           {
               "disease": "d1",
               "treatment": "t1"
           },
           {
               "disease": "d2",
               "treatment": "t2"
           },
           {
               "disease": "d3",
               "treatment": "t3"
           }
       ]
   },
   {
       "firtName": "Jane",
       "lastName": "Doe",
       "age": 28,
       "history": [
           {
               "disease": "d1",
               "treatment": "t1"
           },
           {
               "disease": "d2",
               "treatment": "t2"
           }
       ]
   },
   {
       "firtName": "Joe",
       "lastName": "Simons",
       "age": 42,
       "history": [
           {
               "disease": "d1",
               "treatment": "t1"
           }
       ]
   }])
   ```

2. Actualizar los datos de un paciente con nueva edad, nombre e historial.

   ```MongoDB
   db.patients.updateOne({"_id": ObjectId("60e7126cb57c82afa1f29eb6")}, {$set: {
       "name": "Linus", "age": 34, "history": [{"disease": "cold", "treatment": "t1"}]
   }})
   ```

3. Encontrar todos los paciente mayores de 30 años.

   ```MongoDB
   db.patients.find({"age": {$gt: 30}}).pretty()
   ```

4. Eliminar todos los pacientes que tengan frío como enfermedad.

   ```MongoDB
   db.patients.deleteMany({"history.disease": "cold"})
   ```
