# Ejercicio 6

1. Crear una nueva colección ("sports") y usar el método upsert para insertar dos nuevos documentos (campos: "title" y "requiresTeam").

   ```MongoDB
   db.sports.updateMany({}, {$set: {title: "Football", requiresTeam: true}}, {upsert: true})
   db.sports.updateMany({title: "Running"}, {$set: {requiresTeam: false}}, {upsert: true})
   ```

2. Actualizar todos los documentos que requieran de equipo para añadir un nuevo campo con el mínimo de jugadores requeridos.

   ```MongoDB
   db.sports.updateMany({requiresTeam: true}, {$set: {minPlayers: 11}})
   ```

3. Actualizar todos los documentos que requieran de equipo incrementando en 10 el número de jugadores requeridos.

   ```MongoDB
   db.sports.updateMany({requiresTeam: true}, {$inc: {minPlayers: 10}})
   ```
