# Ejercicio 7

1. Obtener tres puntos en Google Maps y guardarlos en una colección

   ```MongoDB
   db.places.insertOne({name: "Puerta de Toledo", location: {type: "Point", coordinates: [-3.713809, 40.4067654]}})
   db.places.insertOne({name: "Museo Nacional de Antropología", location: {type: "Point", coordinates: [-3.7080584, 40.4198479]}})
   db.places.insertOne({name: "Mercado de San Fernando", location: {type: "Point", coordinates: [-3.7042006, 40.4077339]}})
   ```

2. Coger un nuevo punto y encontrar el punto más cercano dentro de una distancia mínima y máxima

   ```MongoDB
   db.places.createIndex({location: "2dsphere"})
   db.places.find({location: {$near: {$geometry: {type: "Point", coordinates: [-3.705102, 40.407611]}, $minDistance: 50, $maxDistance: 300}}})
   ```

3. Obtener un área y ver que puntos, guardados en la colección, están contenidos en la misma

   ```MongoDB
   db.places.find({location: {$geoWithin: {$geometry: {type: "Polygon", coordinates: [[[-3.705178, 40.408096], [-3.702976, 40.408124], [-3.702633, 40.407054], [-3.704806, 40.406887], [-3.705178, 40.408096]]]}}}})
   ```

4. Guardar al menos un área en otra colección

   ```MongoDB
   db.areas.insertOne({name: "Área 1", area: {type: "Polygon", coordinates: [[[-3.705178, 40.408096], [-3.702976, 40.408124], [-3.702633, 40.407054], [-3.704806, 40.406887], [-3.705178, 40.408096]]]}})
   db.areas.insertOne({name: "Área 2", area: {type: "Polygon", coordinates: [[[-3.705075, 40.407639], [-3.702050, 40.406831], [-3.702557, 40.409029], [-3.705075, 40.407639]]]}})
   ```

5. Coger un nuevo punto y busca qué áreas de la colección contienen el punto

   ```MongoDB
   db.areas.find({area: {$geoIntersects: {$geometry: {type: "Point", coordinates: [-3.703120, 40.408322]}}}})
   ```
