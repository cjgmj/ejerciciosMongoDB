# Ejercicio 9

1. Crear un usuario que pueda trabajar con las bases de datos, colecciones e índices.

   ```MongoDB
   use admin
   db.createUser({user: 'dbAdmin', pwd: 'dbAdmin', roles: ['dbAdminAnyDatabase']})
   ```

2. Crear un usuario que pueda trabajar con los usuarios.

   ```MongoDB
   use admin
   db.createUser({user: 'userAdmin', pwd: 'userAdmin', roles: ['userAdminAnyDatabase']})
   ```

3. Crear un usuario que pueda leer y escribir datos de las bases de datos de "Customers" y "Sales".

   ```MongoDB
   use customers
   db.createUser({user: 'developer', pwd: 'developer', roles: ['readWrite', {role: 'readWrite', db: 'sales'}]})
   ```
