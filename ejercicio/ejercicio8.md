# Ejercicio 8

1. Contruir una agregación para encontrar a las personas mayores de 50, agruparlas por género, contar cuantas personas hay por género, la edad media de las personas y ordenar el resultado por el número de personas.

   ```MongoDB
   db.persons.aggregate([
       {$match: {"dob.age": {$gt: 50}}},
       {$group: {_id: {gender: "$gender"}, numPersons: {$sum: 1}, averageAge: {$avg: "$dob.age"}}},
       {$sort: {numPersons: -1}}
   ])
   ```
