# Ejercicio 4

1. Importar la base de datos y la colección.

   ```MongoDB
   mongoimport boxoffice.json -d boxOffice -c movies --drop --jsonArray
   ```

2. Buscar todas las películas que tengan una puntuación mayor a 9.2 y una duración menos de 100 minutos.

   ```MongoDB
   db.movies.find({"meta.rating": {$gt: 9.2}, "meta.runtime": {$lt: 100}}).pretty()
   ```

3. Buscar todas las películas que tengan el género drama o acción.

   ```MongoDB
   db.movies.find({$or: [{genre: "drama"}, {genre: "action"}]}).pretty()
   ```

4. Buscar todas las películas donde los espectadores superen a los esperados.

   ```MongoDB
   db.movies.find({$expr: {$gt: ["$visitors", "$expectedVisitors"]}}).pretty()
   ```
