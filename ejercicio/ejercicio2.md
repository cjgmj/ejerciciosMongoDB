# Ejercicio 2

1. Insertar usuarios.

   ```MongoDB
   db.users.insertMany([{
       "firtName": "John",
       "lastName": "Doe",
       "age": 33,
       "email": "john.doe@test.com"
   },
   {
       "firtName": "Jane",
       "lastName": "Doe",
       "age": 28,
       "email": "jane.doe@test.com"
   },
   {
       "firtName": "Joe",
       "lastName": "Simons",
       "age": 42,
       "email": "joe.simons@test.com"
   }])
   ```

2. Insertar post.

   ```MongoDB
   db.posts.insertOne({
       "title": "My first Post!",
       "text": "This is my first post",
       "tags": ["new", "tech"],
       "creator": ObjectId("60f032bfddf62f5515436858"),
       "comments": [{
           "text": "I like this post!",
           "author": ObjectId("60f032bfddf62f5515436859")
       },
       {
           "text": "Nice post",
           "author": ObjectId("60f032bfddf62f551543685a")
       }]
   })
   ```

3. Obtener posts con relaciones.

   ```MongoDB
   db.posts.aggregate([{$lookup: {
      from: "users",
      localField: "creator",
      foreignField: "_id",
      as: "postAuthor"
   }},
   {$lookup: {
      from: "users",
      localField: "comments.author",
      foreignField: "_id",
      as: "commentsAuthor"
   }}]).pretty()
   ```
