# Ejercicio 5

1. Importar la colección dentro de la base de datos boxOffice.

   ```MongoDB
   mongoimport boxoffice-extended.json -d boxOffice -c exmovies --drop --jsonArray
   ```

2. Buscar todas las películas que tengan dos géneros.

   ```MongoDB
   db.exmovies.find({genre: {$size: 2}}).pretty()
   ```

3. Buscar todas las películas que salieron en 2018.

   ```MongoDB
   db.exmovies.find({"meta.aired": 2018}).pretty()
   ```

4. Buscar todas las películas que tengan valoraciones superiores a ocho e inferiores a diez.

   ```MongoDB
   db.exmovies.find({ratings: {$elemMatch: {$gt: 8, $lt: 10}}}).pretty()
   ```
